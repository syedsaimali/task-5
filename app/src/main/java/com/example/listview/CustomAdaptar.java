package com.example.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CustomAdaptar extends ArrayAdapter<String> {
    Context context;
    String[] names;
    int[] images;
    LayoutInflater test;

    public CustomAdaptar (Context appContext, String[] names, int[]images){
        super(appContext, R.layout.list_view, names);
        this.context= context;
        this.names = names;
        this.images = images;
        test = (LayoutInflater.from(appContext));
    }

    public View getView(int position, View view, ViewGroup parent){
        view = test.inflate(R.layout.list_view, null);
        TextView teamName = (TextView) view.findViewById(R.id.text);
        ImageView flag = (ImageView) view.findViewById(R.id.image);
        teamName.setText(names[position]);
        flag.setImageResource(images[position]);
        return view;
    }
}
