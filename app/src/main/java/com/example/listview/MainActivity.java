package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String[] names = {"Quetta Gladiator", "Multan Sultans", "Lahore Qalandars", "Islamabad United", "Peshawar Zalmi", "Karachi Kings"};
    int[] images= {
            R.drawable.quettagladiators, R.drawable.multansultan, R.drawable.lahoreqalandar,
            R.drawable.islamabadunited, R.drawable.peshawarzalmi, R.drawable.karachikings
    };
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        CustomAdaptar customAdaptar = new CustomAdaptar(this, names, images);

        listView.setAdapter(customAdaptar);
    }
}